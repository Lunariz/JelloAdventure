Jello Adventure

Warning: This project severely lacks comments. For good commenting style, check out Chaka Panda (https://github.com/Lunariz/ChakaPanda)
Looking for the game? Play Jello Adventure here: http://www.jelloadventure.ga

The Goal
Jello Adventure was created during the 72 hour gamejam called Ludum Dare 33.
The theme of the gamejam was 'You are the monster'
Out of 1526 team entries, Jello Adventure was ranked #62!

The Team
Lukas Donkers			Programmer
Roy Schroder			Programmer
Alexandra Cunetchi		Programmer
Long Zhang				Art & Level Design
Alexander Kappelhoff	Art & Animation
Stan Erbrink			Audio

The Outcome
All in all, we are extremely proud of our game and it's final ranking. We loved reading all comments on the Ludum Dare page (http://ludumdare.com/compo/ludum-dare-33/?action=preview&uid=56884) and we would love to thank everyone who played or playtested it!