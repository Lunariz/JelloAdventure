﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(WaterBlock))]
public class WaterEditor : Editor
{

    void Awake()
    {
        WaterBlock wb = (WaterBlock)target;
        if (wb.waterblocks == null || wb.waterblocks.Count == 0)
        {
            wb.addBlock();
            wb.transform.position = new Vector3(0, 0, .5f);
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        WaterBlock wb = (WaterBlock)target;
        
        if (GUILayout.Button("Add waterbody"))
        {
            wb.addBlock();
        }
        if (wb.waterblocks.Count >= 2)
        {
            if (GUILayout.Button("reduce width"))
            {
                wb.RemoveBlock();
            }
        }
        
        

    }
}
