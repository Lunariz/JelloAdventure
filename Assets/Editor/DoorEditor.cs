﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[CustomEditor(typeof(Door))]
public class DoorEditor : Editor {
    public GameObject door;
    public ExitDoor doorDetails;
    

    public 
	// Use this for initialization
	void Awake () {
        Door d = (Door)target;
        doorDetails = d.GetComponentInChildren<ExitDoor>();

        if (doorDetails == null)
        {
            door = GameObject.Instantiate(Resources.Load("Door/DoorWithSign")) as GameObject;
            door.transform.parent = d.transform; 
            doorDetails = door.GetComponentInChildren<ExitDoor>();
        }

        door = doorDetails.transform.parent.parent.gameObject;

       
        door.transform.parent = d.transform;
	}

   
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnInspectorGUI()
    {

        if (doorDetails.startOpen)
        {
            GUILayout.Label("Status: open on Start");
            if (GUILayout.Button("Add fusebox"))
            {
                createFuseBox();
                
            }
        }
        else
        {
            GUILayout.Label("Status: Closed on Start");
            GUILayout.Label("current trigger count:"+doorDetails.fuseboxes.Count);

            GUILayout.Label("Closing the door at the start removes all fuseboxes");
            if (GUILayout.Button("Remove all Fuseboxes"))
            {
                foreach (FuseBox fb in doorDetails.fuseboxes)
                {
                    DestroyImmediate(fb.gameObject.transform.parent.gameObject,true);

                }
                doorDetails.fuseboxes.Clear();
                doorDetails.startOpen = true;
            }
        }
    }

    private void createFuseBox()
    {
        GameObject fbo = GameObject.Instantiate(Resources.Load("Door/fuse-box")) as GameObject;
        FuseBox fb = fbo.GetComponentInChildren<FuseBox>();
        doorDetails.fuseboxes.Add(fb);
        fb.toUnlock = doorDetails;
        doorDetails.startOpen = false;
        fbo.transform.parent = door.transform;
    }
}
