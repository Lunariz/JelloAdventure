﻿using UnityEngine;
using UnityEditor;
 
public class MenuItems
{
    [MenuItem("Tools/Clear Progression Data")]
    private static void clearAll()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Tools/Create Teleport Pipe &t") ]
    private static void CreateTeleportPipe()
    {
        GameObject pipe = new GameObject();
        pipe.name = "Teleport Pipe";
        pipe.AddComponent<PipeSystem>();
        Selection.activeObject = pipe;

    }

    [MenuItem("Tools/Create Door &d")]
    private static void CreateDoor()
    {
        GameObject door = new GameObject();
        door.name = "Door";
        door.AddComponent<Door>();
        //door.transform.position = new Vector3(0, 0, -1);
        Selection.activeObject = door;
    }

    [MenuItem("Tools/Create WaterBlock &w")]
    private static void CreateWater()
    {
        GameObject water = new GameObject();
        water.name = "MovingWaterBlock";
        water.AddComponent<WaterBlock>();
        //door.transform.position = new Vector3(0, 0, -1);
        Selection.activeObject = water;
    }
}