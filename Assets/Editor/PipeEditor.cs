﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public enum Direction
{
    up = 0,
    left = 1,
    down = 2,
    right = 3,
}

public enum PipeTrigger
{
    PermanentSwitch = 0,
    TemporarySwitch = 1,
}

[CustomEditor(typeof(PipeSystem))]
public class PipeEditor : Editor {
    private Direction CurrentDirection;
    private GameObject previouspiece;
    private PipeTrigger pipetiggertype;

    void Awake()
    {
        
        PipeSystem ps = (PipeSystem)target;
        if (ps.pipes.Count == 0)
        {
            ps.gameObject.transform.position = new Vector3(0, 0, 0);
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/Enterence") as GameObject));
        }
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnInspectorGUI()
    {
        PipeSystem ps = (PipeSystem)target;
        base.OnInspectorGUI();
        
        if (GUILayout.Button("Left"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/CornerPieceL") as GameObject));
            if (CurrentDirection != Direction.right)
                CurrentDirection++;
            else
                CurrentDirection = 0;
        }
        if (GUILayout.Button("Continue"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/PipeMid") as GameObject));
        }
        if (GUILayout.Button("Right"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/CornerPieceR") as GameObject));
            if (CurrentDirection != Direction.up)
                CurrentDirection--;
            else
                CurrentDirection = (Direction)3;
            
        }
        if (GUILayout.Button("End Cap"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/Exit") as GameObject));
        }

        if (ps != null && ps.pipes != null && ps.pipes.Count != 1)
        {
            if (GUILayout.Button("Remove Last"))
            {

            }
        }

        if (GUILayout.Button("Update All Assets"))
        {
            foreach (Pipe p in ps.pipes)
            {

            }
        }

        if (ps.activated)
        {
            GUILayout.Label("Status: Teleport pipe ON");
            
            pipetiggertype= (PipeTrigger)EditorGUILayout.EnumPopup("Pipe Trigger: ",pipetiggertype );
            if (GUILayout.Button("Add Trigger"))
            {
                
            }
        }
        else
        {
            GUILayout.Label("Status: Teleport pipe OFF");

            pipetiggertype = (PipeTrigger)EditorGUILayout.EnumPopup("Pipe Trigger: ", pipetiggertype);
            if (GUILayout.Button("Add Trigger"))
            {

            }
        }
    }
    
    private void positionPieces(GameObject go){
        PipeSystem ps = (PipeSystem)target;
        go.transform.parent = ps.transform;
        go.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90 * (int)CurrentDirection));
        if (previouspiece != null)
        {
            if (CurrentDirection == Direction.up)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(0, 1, 0);
            }
            else if (CurrentDirection == Direction.down)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(0, -1, 0);
            }
            else if (CurrentDirection == Direction.left)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(1, 0, 0);
            }
            else if (CurrentDirection == Direction.right)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(-1, 0, 0);
            }
        }
        else
        {
            go.transform.position = new Vector3(0, 0, 0);
        }
        previouspiece = go ;
        ps.pipes.Add(go.GetComponentInChildren<Pipe>());
    }
}
