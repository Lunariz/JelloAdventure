﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Rope))]
public class RopeEditor :Editor {
    private Direction CurrentDirection;
	// Use this for initialization
	void Start () {
	
	}

    void Awake()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void OnInspectorGUI()
    {
        Rope ro = (Rope)target;
        base.OnInspectorGUI();
        ro.burnTime = EditorGUI.FloatField(new Rect(3,3,150, 20),
						"Rope Burns in:", 
						ro.burnTime);

        if (GUILayout.Button("Left"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/CornerPieceL") as GameObject));
            if (CurrentDirection != Direction.right)
                CurrentDirection++;
            else
                CurrentDirection = 0;
        }
        if (GUILayout.Button("Continue"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/PipeMid") as GameObject));
        }
        if (GUILayout.Button("Right"))
        {
            positionPieces(GameObject.Instantiate(Resources.Load("Pipes/CornerPieceR") as GameObject));
            if (CurrentDirection != Direction.up)
                CurrentDirection--;
            else
                CurrentDirection = (Direction)3;

        }
        

    }
    private void positionPieces(GameObject go)
    {
        PipeSystem ps = (PipeSystem)target;
        go.transform.parent = ps.transform;
        go.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -90 * (int)CurrentDirection));
       /* if (previouspiece != null)
        {
            if (CurrentDirection == Direction.up)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(0, 1, 0);
            }
            else if (CurrentDirection == Direction.down)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(0, -1, 0);
            }
            else if (CurrentDirection == Direction.left)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(1, 0, 0);
            }
            else if (CurrentDirection == Direction.right)
            {
                go.transform.position = previouspiece.transform.position + new Vector3(-1, 0, 0);
            }
        }
        else
        {
            go.transform.position = new Vector3(0, 0, 0);
        }
        previouspiece = go;
        ps.pipes.Add(go.GetComponentInChildren<Pipe>());
        * */
    }
}
