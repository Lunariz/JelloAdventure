﻿using UnityEngine;
using System.Collections;

public class TriggerableObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public virtual void OnTriggered()
    {
        Debug.LogWarning("THIS FUNCTION IS NOT IMPLEMENTED IN BASECLASS");
    }
}
