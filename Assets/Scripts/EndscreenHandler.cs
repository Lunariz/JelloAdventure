﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EndscreenHandler : MonoBehaviour {

    public CircleFaceGUI Circle1;
    public CircleFaceGUI Circle2;
    public CircleFaceGUI Circle3;

    public float fillTarget;
    public Image fillImage;

    public float Target1;
    public float Target2;
    public float Target3;

    private bool soundonce = false;

    public static EndscreenHandler instance;

    void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    void Update()
    {
        if (fillTarget > fillImage.fillAmount)
        {
            fillImage.fillAmount += 0.4f * Time.deltaTime;

            if (fillImage.fillAmount < Target1)
            {
                Circle1.ShowFace(null);
                Circle2.ShowFace(null);
                Circle3.ShowFace(null);
            }
            else if (fillImage.fillAmount < Target2)
            {
                Circle1.ShowFace(Circle1.Sadface);
                Circle2.ShowFace(null);
                Circle3.ShowFace(null);
            }
            else if (fillImage.fillAmount < Target3)
            {
                Circle1.ShowFace(Circle1.Normalface);
                Circle2.ShowFace(Circle2.Normalface);
                Circle3.ShowFace(null);
            }
            else if (fillImage.fillAmount > Target3)
            {
                Circle1.ShowFace(Circle1.Happyface);
                Circle2.ShowFace(Circle2.Happyface);
                Circle3.ShowFace(Circle3.Happyface);
                
            }
        }
    }

    public void ShowScreen(float size, float t1 = 0f, float t2 = 0.5f, float t3 = 0.9f)
    {
        if (!PlayerPrefs.HasKey("Level" + ((int)LevelHandler.instance.currentLevel.LevelEnum + 1)))
        {
            PlayerPrefs.SetInt("Level" + ((int)LevelHandler.instance.currentLevel.LevelEnum + 1), 0);

        }
        CanvasHandler.instance.ShowScreen(CanvasHandler.instance.EndScreen);
        fillTarget = size / 100f;
        fillImage.fillAmount = 0;
        Target1 = t1;
        Target2 = t2;
        Target3 = t3;
        soundonce = false;
    }

    public void CloseScreen()
    {
        if (StorySign.instance.introStory.Count != 0)
        {
            CanvasHandler.instance.ShowScreen(CanvasHandler.instance.TutorialScreen);
        }
        else
        {
            CanvasHandler.instance.ShowScreen(null);
        }
    }

    public void Continue()
    {
        LevelHandler.instance.GotoNextLevel();
    }
}
