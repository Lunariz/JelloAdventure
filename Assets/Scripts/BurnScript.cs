﻿using UnityEngine;
using System.Collections;

public class BurnScript : MonoBehaviour {
    private bool onMobile;
    void Awake()
    {
        this.transform.parent.parent.GetComponent<Rope>().burn += Burn;
        if (Application.platform != RuntimePlatform.WebGLPlayer) onMobile = true;
    }
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Burn()
    {
        foreach (Transform t in GetComponentsInChildren<Transform>(true)) 
        {
            t.gameObject.SetActive(true);
            if (onMobile)
            {
                foreach (ParticleSystem p in t.GetComponentsInChildren<ParticleSystem>())
                {

                    p.emissionRate = 20;
                }
            }
            else
            {
                foreach (ParticleSystem p in t.GetComponentsInChildren<ParticleSystem>())
                {

                    p.emissionRate = 20;
                }
                
            }
        }
    }
}
