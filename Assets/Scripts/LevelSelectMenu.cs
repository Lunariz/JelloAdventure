﻿using UnityEngine;
using System.Collections;

public class LevelSelectMenu : MonoBehaviour {
    public GameObject levelButton;
    public Transform grid;
    void Awake()
    {
        if (!PlayerPrefs.HasKey("Level0"))
        {
            PlayerPrefs.SetInt("Level0", 0);
        }
    }
	// Use this for initialization
	void Start () {
        int l = 0;
        foreach (Level level in LevelHandler.instance.levels)
        {
            GameObject g = GameObject.Instantiate(levelButton);
            g.transform.parent = grid;
            g.GetComponent<LevelSelectButton>().SetButtonDetails(l);
            l++;
        }
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
