﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public enum GameSceneState{
    Menu,
    Game
}


public class AudioManager : MonoBehaviour {
    // sfx clips should be in order of the soundclip enum
    public List<AudioClip> SFX;
    // multiple DeathSOunds
    public List<AudioClip> SFXDeaths; 
    // this is the target to send your audio play too;
    public static AudioManager instance = null;
    // setting for in the menu to turn sound on or off
    public bool AllowSound = true;

    //private list to find the first free audio source
    private List<AudioSource> sources = new List<AudioSource>();

    // music Layer
    private AudioSource music;

    // Set gameMusic
    public AudioClip backgroundMusic;
    // set background music
    public AudioClip menuMusic;

    public List<AudioClip> footStepSounds;

    private AudioSource footSteps = new  AudioSource();
    
    public enum SoundClips{
        //Jump,
        //Idle,
        BurnRope = 0,
        Death = 1,
        ElectricLoop = 2,
        SwitchToElectric =3,
        SwitchToWater = 4,
        SwitchToOil = 5,
        SwitchToFire = 6,
        Victory = 7,
        ObjectDropped = 8,
        JumpLand = 9,
        FuseBox = 10

    }

    void Awake()
    {
        if (AudioManager.instance == null)
        {
            AudioManager.instance = this;
            DontDestroyOnLoad(this);
            music = this.gameObject.AddComponent<AudioSource>();
            footSteps = this.gameObject.AddComponent<AudioSource>();
            
                
            
            
        }
        else
        {
            Destroy(this);
        }

        
    }

    
    
    void Update(){
        if(Camera.main != null)
    	transform.position = Camera.main.transform.position;
    }

    public void ToggleMute()
    {
        AllowSound = !AllowSound;
        if (AllowSound)
        {
            CanvasHandler.instance.StartMenu.transform.GetChild(1).GetComponentInChildren<Text>().text = "Mute";
            music.Play();
        }
        else
        {
            CanvasHandler.instance.StartMenu.transform.GetChild(1).GetComponentInChildren<Text>().text = "Unmute";
            music.Stop();
        }
    }

    /// <summary>
    /// plays the sound that is listed by the name
    /// </summary>
    /// <param name="soundName"></param>
	public void PlaySound(SoundClips soundName,bool shouldPitch)
    {
        if (AllowSound)
        {
        
            /*if (soundName == SoundClips.Death)
            {
                AudioSource a = findFirstFreeAudioSource();
                a.pitch = Random.Range(0.8f, 1.2f);
                
                a.PlayOneShot(SFXDeaths[Random.Range(0, SFXDeaths.Count)]);
            }*/
            
            if (shouldPitch)
            {

                
            AudioSource a = findFirstFreeAudioSource();
            a.pitch = Random.Range(0.9f, 1.1f);
            a.PlayOneShot(SFX[(int)soundName]);
                    
            }
            else
            {
                AudioSource a = findFirstFreeAudioSource();
                a.pitch = 1;
                a.volume = .6f;    
                a.PlayOneShot(SFX[(int)soundName]);
            }
            
        }
    }

   
    /// <summary>
    ///  looks for first free audio source, or creates it
    /// </summary>
    /// <returns>returns free audio sources</returns>
    private AudioSource findFirstFreeAudioSource()
    {
        foreach (AudioSource aus in sources)
        {
            if (!aus.isPlaying)
            {
                aus.volume = 1;
                return aus;
            }
        }

        AudioSource newSoundSource = this.gameObject.AddComponent<AudioSource>();
        sources.Add(newSoundSource);
        return newSoundSource;
    }

    /// <summary>
    /// plays music if music is turned on
    /// </summary>
    /// <param name="state"></param>
    public void PlayMusic(GameSceneState state)
    {
        if (AllowSound)
        {
            //if(music!= null && music.isPlaying)music.Stop();
            if (state == GameSceneState.Menu) music.clip = menuMusic;
            else if (state == GameSceneState.Game) music.clip = backgroundMusic;
            music.loop = true;
            music.Play();
        }
    }


    /// <summary>
    /// plays footstep sounds
    /// </summary>
    /// <param name="isWalking">is the unit touching the ground, and moving</param>
    /// <param name="state">What form is the unit in, decides sound effect for the step sounds</param>
    public void toggleFoodSteps(bool isWalking,SlimeState state)
    {
        if (footSteps.clip != footStepSounds[(int)state])
        {
            footSteps.clip = footStepSounds[(int)state];   
            
        }
        if (AllowSound)
        {
            if (isWalking)
            {
                if (!footSteps.isPlaying) footSteps.Play();
            }
            else footSteps.Stop();
        }
    }

}
