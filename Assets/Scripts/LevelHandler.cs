﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelHandler : MonoBehaviour {

    public static LevelHandler instance;

    public PlayerController player;
    public Level currentLevel;
    public List<Level> levels;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
        
    }

    
	// Use this for initialization
	void Start ()
    {

	}

    public void StartGame()
    {
        if (currentLevel) Destroy(currentLevel.gameObject);
        CanvasHandler.instance.ShowScreen(null);
        AudioManager.instance.PlayMusic(GameSceneState.Game);
        //StorySign.instance.ShowIntroductions();
        currentLevel = GameObject.Instantiate<Level>(levels[0]);
        currentLevel.Initialize(player, Levels.Level1);
    }

    private bool deathPlayedOnce = false;
    public void ResetLevel()
    {
        if (!deathPlayedOnce)
        {
            AudioManager.instance.PlaySound(AudioManager.SoundClips.Death, false);
            Invoke("ResetLevelPostSound", 0.5f);
            deathPlayedOnce = true;
        }
    }

    public void ResetLevelPostSound()
    {
        Levels currentLevelEnum = currentLevel.LevelEnum;
        Destroy(currentLevel.gameObject);
        currentLevel = GameObject.Instantiate<Level>(levels[(int) currentLevelEnum]);
        currentLevel.Initialize(player, currentLevelEnum);
        deathPlayedOnce = false;
        //CanvasHandler.instance.ShowScreen(null);
    }

    public void GotoNextLevel()
    {
        Levels currentLevelEnum = currentLevel.LevelEnum;
        if ((int)currentLevelEnum + 1 < levels.Count)
        {
            Levels nextLevelEnum = currentLevelEnum + 1;
            Destroy(currentLevel.gameObject);
            currentLevel = GameObject.Instantiate<Level>(levels[(int)nextLevelEnum]);
            currentLevel.Initialize(player, nextLevelEnum);
        }
        else
        {
            Application.LoadLevel(Application.loadedLevel);
        }
        
        EndscreenHandler.instance.CloseScreen();
    }

    public void GotoLevel(int levelId)
    {
        if (levelId == 0)
        {
            StartGame();
            return;
        }
        if (currentLevel) Destroy(currentLevel.gameObject);
        CanvasHandler.instance.ShowScreen(null);
        AudioManager.instance.PlayMusic(GameSceneState.Game);
        //StorySign.instance.ShowIntroductions();
        currentLevel = GameObject.Instantiate<Level>(levels[levelId]);
        currentLevel.Initialize(player, (Levels)levelId);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

public enum Levels
{
    Level1,
    Level2,
    Level3,
    Level4,
    Level5,
    Level6,
    Level7,
    Level8,
    Level9,
    Level10,
    Level11,
    Level12,
    Level13,
}
