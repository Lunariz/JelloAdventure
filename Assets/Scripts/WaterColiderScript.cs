﻿using UnityEngine;
using System.Collections;

public class WaterColiderScript : MonoBehaviour {
    public float wavemovement;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y + Mathf.Sin(Time.time) * wavemovement, this.transform.position.z);
	
	}
}
