﻿using UnityEngine;
using System.Collections;

public class ExitSign : MonoBehaviour {
    public Material doorOpen;
    public Material doorClosed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setDoor(bool open)
    {
        if (open)
        {
            GetComponent<Renderer>().material = doorOpen;
        }
        else
        {
            GetComponent<Renderer>().material = doorClosed;
        }
    }
}
