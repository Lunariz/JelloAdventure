﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectButton : MonoBehaviour {
    public int loadLevel;
    public bool unlocked = false;
    public int Score = 0;
    public Text lbl;
    public Button button;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void SetButtonDetails(int level)
    {
        loadLevel = level;
        lbl.text = ""+(level+1);
        if (PlayerPrefs.HasKey("Level"+level))
        {
            unlocked = true;
            Score = PlayerPrefs.GetInt("Level" + level);
            button.interactable = true;

        }
    }

    public void OnClick()
    {
        LevelHandler.instance.GotoLevel(loadLevel);
    }
}
