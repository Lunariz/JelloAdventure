﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

class FollowPlayer : MonoBehaviour
{
    public float zDistance = 3.155f;
    public float sideOffset = 3;
    public float verticalOffset = 2f;
    public static FollowPlayer instance;
    public PlayerController playerToFollow;

    void Awake()
    {
        instance = this;
        gameObject.transform.position = new Vector3(0, 0, zDistance);
    }

    void Update()
    {
        if (playerToFollow != null && !CanvasHandler.instance.CurrentScreen)
        {
            //oud
            //gameObject.transform.position = playerToFollow.transform.position + new Vector3(0, 0, zDistance);

            Vector2 horizontalDifference = new Vector2(playerToFollow.transform.position.x, 0) - new Vector2(gameObject.transform.position.x, 0);
            Vector2 verticalDifference = new Vector2(0, playerToFollow.transform.position.y) - new Vector2(0, gameObject.transform.position.y);
            if (horizontalDifference.magnitude > sideOffset)
            {
                float amount = horizontalDifference.magnitude - sideOffset;
                horizontalDifference.Normalize();
                horizontalDifference *= amount;
                Vector3 V3CameraDifference = new Vector3(horizontalDifference.x, 0, 0);
                gameObject.transform.position = gameObject.transform.position + V3CameraDifference;
            }
            if (verticalDifference.magnitude > verticalOffset)
            {
                float amount = verticalDifference.magnitude - verticalOffset;
                verticalDifference.Normalize();
                verticalDifference *= amount;
                Vector3 V3CameraDifference = new Vector3(0, verticalDifference.y, 0);
                gameObject.transform.position = gameObject.transform.position + V3CameraDifference;
            }
        }
    }

    public void PlayerInBottomleft()
    {
        gameObject.transform.position = gameObject.transform.position + new Vector3(-5, 10, 0);
    }
}

