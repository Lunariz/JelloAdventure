﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

    public Vector3 PlayerStartPosition;
    public PlayerController Player;
    public Levels LevelEnum;

    public void Initialize(PlayerController _player, Levels _LevelEnum)
    {
        if (FindObjectsOfType<SignScript>().Length != 0)
        {
            StorySign.instance.ShowIntroductions();
        }
        if (_player == null)
        {
            _player = FindObjectOfType<PlayerController>();
        }
        Player = _player;
        Player.transform.position = PlayerStartPosition;
        Player.Reset();
        LevelEnum = _LevelEnum;
        FollowPlayer.instance.PlayerInBottomleft();
    }

	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
