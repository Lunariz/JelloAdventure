﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PlayerController : MonoBehaviour {
    public SkeletonAnimation frontSpine;
    public SkeletonAnimation SideSpine;
    //public static PlayerController instance;

    public bool Active = true;
    public PlayerController otherPlayer = null;

    public List<string> stateMaterials;

    public float acceleration;
    public Vector2 accelDirection = new Vector2(0, 0);
    public float speed;
    public Vector2 speedDirection = new Vector2(0, 0);

    public bool jumping = false;
    public GameObject standingOn;

    //Player settings
    public float moveSpeed = 0.5f;
    public float maxSpeed = 5f;
    public float jumpHeight = 425;
    public SlimeState state = SlimeState.Normal;
    public float size = 100;
    public float maxSize = 100;
    public bool light { get; set; }

    public bool stateChanged;
    public float targetMoveSpeed = 0.5f;
    public float targetHealth = -1;
	private Vector3 prevPos;

    public float animationDelay;

    private List<ContactPoint> currentContactPoints = new List<ContactPoint>();

    private Vector3 touchStart;
    private Vector3 touchEnd;
    private bool swipeMoveLeft;
    private bool swipeMoveRight;
    private bool swipeMoveUp;
    private bool mouseOver;

    public bool spiked = false;

    public List<ParticleSystem> stateParticles;

    public List<Color32> RGBList1;


    void Awake()
    {
        //moveSpeed = 0.5f;
        //axSpeed = 5f;
        //jumpHeight = 325f;
        //state = SlimeState.Normal;
        Physics.gravity = new Vector3(0, -15, 0);
    }

	// Use this for initialization
	void Start() {
	    //Subscribe to state changing events
        FollowPlayer.instance.playerToFollow = this;
	}

    private bool wasJumping = false;
    // Update is called once per frame
    private bool alreadyswimming = false;
    void Update() {
        
        
        if (alreadyswimming != swimming)
        {
            if (swimming)
            {
                SideSpine.AnimationName = "swim " + stateMaterials[(int)state];
            }
            else
            {
                SideSpine.AnimationName = "run " + stateMaterials[(int)state];
            }
            alreadyswimming = swimming;
        }
        if (wasJumping!=jumping)
        {
            if (wasJumping)
            {
                AudioManager.instance.PlaySound(AudioManager.SoundClips.JumpLand, false);
            }
            wasJumping = jumping;
            //something changed

        }
        if (CanvasHandler.instance.CurrentScreen != null||StorySign.instance.gameObject.activeSelf) return;
        if (animationDelay > 0)
        {
            animationDelay -= Time.deltaTime;
        }
        else
            CheckSwipe();
        
        
        Move();
        HandleStates();
        CheckDeath();
        
        if (Input.GetKey(KeyCode.Space)){
            Split();
        }

        if (otherPlayer )
        {
            if (Active)
            {
                if (mayMerge()) Merge();
            }
            HUDHandler.instance.SetSlider(size, otherPlayer.size);
            HUDHandler.instance.SetColors(RGBList1[(int)this.state], RGBList1[(int)otherPlayer.state]);
        }
        else  {
            HUDHandler.instance.SetSlider(size);
            HUDHandler.instance.SetColors(RGBList1[(int)this.state]);
        }
    }

    #region movement

    void CheckSwipe()
    {
        foreach (Touch touch in Input.touches)
        {
            HandleTouch(touch.fingerId, Camera.main.ScreenToWorldPoint(touch.position), touch.phase);
        }

        // Simulate touch events from mouse events
        #if UNITY_EDITOR
        if (Input.touchCount == 0)
        {
            if (Input.GetMouseButtonDown(0))
            {
                HandleTouch(10, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Began);
            }
            if (Input.GetMouseButton(0))
            {
                HandleTouch(10, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Moved);
            }
            if (Input.GetMouseButtonUp(0))
            {
                HandleTouch(10, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Ended);
            }
        } 
#endif
    }

    void HandleTouch(int touchFingerId, Vector3 touchPosition, TouchPhase touchPhase)
    {
        if (touchPhase == TouchPhase.Began)
        {
            touchStart = touchPosition;
        }
        if (touchPhase == TouchPhase.Moved)
        {
            touchEnd = touchPosition;
            if (!Swipe(touchStart, touchEnd, false))
            {
                touchStart = touchEnd;
            }
        }
        if (touchPhase == TouchPhase.Ended)
        {
            touchEnd = touchPosition;
            Swipe(touchEnd, touchEnd);
        }
    }

    //Note: x direction is reversed!
    bool Swipe(Vector3 start, Vector3 end, bool allowTap = true)
    {
        swipeMoveUp = false;
        Vector3 fullDirection = (end - start);
        Vector3 direction = fullDirection.normalized;
        if (!allowTap && fullDirection.magnitude < 1f) return true;
        if (direction.x > 0.5f) //left
        {
            swipeMoveRight = false;
            swipeMoveLeft = true;
        }
        else if (direction.x < -0.5f) //right
        {
            swipeMoveLeft = false;
            swipeMoveRight = true;
        }
        if (direction.y > 0.5f)
        {
            swipeMoveUp = true;
        }
        if (direction.y < 0.5f * -Mathf.Sqrt(3) || (fullDirection.magnitude < 0.5f && allowTap))
        {
            swipeMoveLeft = false;
            swipeMoveRight = false;
            swipeMoveUp = false;
            if (fullDirection.magnitude < 0.5f)
            {
                TapOnObject();
            }
            return true;
        }
        return false;
    }

    void TapOnObject()
    {
        if (mouseOver) Split();
        if (otherPlayer && otherPlayer.mouseOver)
        {
            Active = false;
            otherPlayer.setActive();
        }
        //if (currentSign && currentSign.GetComponent<SignScript>().mouseOver)
        //{
        //    Debug.Log("tappin");
            
            
        //}
    }

    void OnMouseEnter()
    {
        mouseOver = true;
    }

    void OnMouseExit()
    {
        mouseOver = false;
    }

    void Move()
    {
        if (!Active)
        {
            return;
        }
        acceleration = 0f;
        accelDirection = new Vector2(0, 0);
        if (Input.GetKey("d") || Input.GetKey("right") || swipeMoveRight)
        {
            resetMergeTimer();
            if (SideSpine.transform.localScale.x > 0) SideSpine.transform.localScale = new Vector3(SideSpine.transform.localScale.x * -1, SideSpine.transform.localScale.y);
            if (frontSpine.transform.localScale.x > 0) frontSpine.transform.localScale = new Vector3(frontSpine.transform.localScale.x * -1, frontSpine.transform.localScale.y);
            acceleration = moveSpeed;
            accelDirection += new Vector2(-1, 0);
        }
        if (Input.GetKey("a") || Input.GetKey("left") || swipeMoveLeft)
        {
            resetMergeTimer();
            if (SideSpine.transform.localScale.x < 0) SideSpine.transform.localScale = new Vector3(SideSpine.transform.localScale.x * -1, SideSpine.transform.localScale.y);
            if (frontSpine.transform.localScale.x < 0) frontSpine.transform.localScale = new Vector3(frontSpine.transform.localScale.x * -1, frontSpine.transform.localScale.y);
            acceleration = moveSpeed;
            accelDirection += new Vector2(1, 0);
        }
        if (Input.GetKey("w") || Input.GetKey("up") || swipeMoveUp)
        {
            if (!jumping)
            {
                resetMergeTimer();
                Vector3 velocity = gameObject.GetComponent<Rigidbody>().velocity;
                velocity.y = 0;
                gameObject.GetComponent<Rigidbody>().velocity = velocity;
                gameObject.GetComponent<Rigidbody>().AddForce(0, jumpHeight, 0);
                jumping = true;
                standingOn = null;
                swipeMoveUp = false;
            }
        }
        if (acceleration > 0)
        {
            if (speedDirection.x / accelDirection.x >= 0) speed = Mathf.Min(speed + acceleration, maxSpeed);
            else speed = Mathf.Max(speed - acceleration, 0);
            speedDirection = speedDirection.normalized * speed + (accelDirection.normalized * acceleration * Time.deltaTime);
        }
        else
        {
            speed = Mathf.Max(speed * 0.85f, 0.0001f);
            speedDirection *= 0.85f;
            if (speedDirection.magnitude < 0.0001f) speedDirection = new Vector2(0, 0);
        }
        if (speed > 0)
        {
            //Debug.Log(speed);
           
            Vector2 currentPosition = gameObject.transform.position;
            Vector2 newPosition = currentPosition + (speed * speedDirection.normalized * Time.deltaTime);
            Vector3 new3DPosition = new Vector3(newPosition.x, newPosition.y, 0);
            gameObject.transform.position = new3DPosition;
            gameObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        if (speed < 1f)
        {

            SideSpine.GetComponent<Renderer>().enabled = false;
            frontSpine.GetComponent<Renderer>().enabled = true;
        }
        else
        {
            SideSpine.GetComponent<Renderer>().enabled = true;
            frontSpine.GetComponent<Renderer>().enabled = false;
        }

        if (Input.GetKey("r"))
        {
            ResetLevel();
        }

        if (Input.GetKey("n"))
        {
            HitGoal();
        }

        bool wallCollision = false;
        List<ContactPoint> toDelete = new List<ContactPoint>();
        Vector3 bounceDirection = new Vector3(0, 0, 0);
        float averageX = 0f;
        float averageY = 0f;
		Vector4 hitPointSide = new Vector4(0, 0, 0, 0);
        GameObject hitObject = this.gameObject; // gets changed
        foreach (ContactPoint cp in currentContactPoints)
        {
            if (cp.normal.y < Mathf.Sqrt(2f) / 2f)
            //if (cp.normal.y < Mathf.Sqrt(2f) / 2f && cp.normal.y > -Mathf.Sqrt(2f) / 2f)
            {
                Vector2 currentPosition = gameObject.transform.position;
                Vector2 newPosition;

                Vector3 bounceOffset = new Vector3(cp.normal.x * this.GetComponent<BoxCollider>().size.x / 2f * this.transform.localScale.x, cp.normal.y * this.GetComponent<BoxCollider>().size.y / 2f * this.transform.localScale.y, 0);
                newPosition = cp.point + bounceOffset;
                bounceDirection += cp.normal;
                averageX += newPosition.x;
                averageY += newPosition.y;
				hitPointSide += new Vector4(cp.point.x, cp.point.y, cp.point.z, cp.normal.x);
                if (cp.otherCollider) hitObject = cp.otherCollider.gameObject;
                wallCollision = true;
                if (!jumping)
                {
                    swipeMoveLeft = false;
                    swipeMoveRight = false;
                }
                toDelete.Add(cp);
            }
            
        }

        if (averageY != 0) averageY /= toDelete.Count;
        if (averageX != 0) averageX /= toDelete.Count;
		if(toDelete.Count !=0){
			hitPointSide /= toDelete.Count;
            if(!hitObject.CompareTag("Splat")&&!hitObject.CompareTag("Player")&&!hitObject.CompareTag("InteractableObject")) this.GetComponent<SlimeTrail>().LeaveSplatSide(hitPointSide, hitObject); //experimental on ceiling: , bounceDirection);
		}
        this.transform.position = new Vector3(((averageX == 0) ? this.transform.position.x : averageX), ((averageY == 0) ? this.transform.position.y : averageY), this.transform.position.z);

        //if (wallCollision) speedDirection.x *= -1f;
        if (wallCollision) speedDirection = Vector3.Reflect(speedDirection, bounceDirection.normalized);

        foreach (ContactPoint cp in toDelete)
        {
            currentContactPoints.Remove(cp);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            if (otherPlayer == null) return;
            Switch();
            //return;
        }
        
	}

    public void setActive()
    {
        FollowPlayer.instance.playerToFollow = this;
        Active = true;
    }

    private void Split()
    {
        if (!Active)
        {
            return;
        }
        if (size > 62.5f)
        {
            size = size / 2f;
            GameObject copy = GameObject.Instantiate(this.gameObject);
            transform.position = new Vector3(this.transform.position.x + .25f, this.transform.position.y);
            copy.transform.position = new Vector3(this.transform.position.x - .5f, this.transform.position.y);
            otherPlayer = copy.GetComponent<PlayerController>();
            otherPlayer.otherPlayer = this;
            this.maxSize = 50f;
            otherPlayer.maxSize = 50f;
            otherPlayer.size -= 0.1f;
            Active = false;
        }
    }

    public void Switch()
    {
        if (Active)
        {
            if (otherPlayer)
            {
                this.Active = false;
                otherPlayer.Invoke("setActive", 0.01f);
            }
        }
        else
            otherPlayer.Switch();
    }

    void OnCollisionEnter(Collision collision)
    {
        currentContactPoints = new List<ContactPoint>();
        foreach (ContactPoint cp in collision.contacts) {
            currentContactPoints.Add(cp);
        }

        BoxCollider thisBox = this.GetComponent<BoxCollider>();
        float thisScale = transform.localScale.y;
        foreach (ContactPoint cp in collision.contacts)
        {
            BoxCollider otherBox = cp.otherCollider.GetComponent<BoxCollider>();
            float otherScale = cp.otherCollider.transform.localScale.y;
            if (cp.normal.y >= Mathf.Sqrt(2f) / 2f || (otherBox != null && Mathf.Abs(cp.point.y - otherBox.transform.position.y - otherScale) < 0.02f))
            {
                standingOn = cp.otherCollider.gameObject;
                currentContactPoints.Remove(cp);
                jumping = false;
            }
            else
            {
                jumping = true;
            }
        }
    }

    #endregion

    #region states
    public void ChangeState(SlimeState state,bool firstRun = false,bool isSwimming = false)
    {
        if (state == SlimeState.Water && this.state == SlimeState.inWater || state == SlimeState.inWater && this.state == SlimeState.Water)
        {
            
        }
        else
        {
            if (!firstRun)
            {
                stateParticles[0].gameObject.SetActive(false);
                stateParticles[0].gameObject.SetActive(true);
            }
        }
        if (this.state != SlimeState.Normal)
        {
            stateParticles[(int)this.state].gameObject.SetActive(false);
        }
        if (state != SlimeState.Normal)
        {
            stateParticles[(int)state].gameObject.SetActive( true);
        }
        this.state = state;

        if (swimming)
        {
            SideSpine.AnimationName = "swim " + stateMaterials[(int)state];
        }
        else
        {
            SideSpine.AnimationName ="run "+ stateMaterials[(int)state];
        }
        
        frontSpine.AnimationName = "idle " + stateMaterials[(int)state];
        stateChanged = true;
    }
    private bool swimming =false;
    public void Swimming(bool _swimming)
    {
        swimming = _swimming;
        //SideSpine.AnimationName = "swim " + stateMaterials[(int)state];
    }

    void HandleStates()
    {
        if (stateChanged)
        {
            targetHealth = -1f;
            targetMoveSpeed = 0.5f;
            maxSpeed = 5f;
            jumpHeight = 425;
            light = false;

            if (state == SlimeState.Fire)
            {
                targetHealth = 0;
                targetMoveSpeed = 0.75f;
                maxSpeed = 7.5f;
            }
            else if (state == SlimeState.Oil)
            {
                targetMoveSpeed = 0.25f;
                maxSpeed = 2.5f;
            }
            else if (state == SlimeState.Water)
            {
                jumpHeight = 350;
                targetHealth = -1f;
            }
            else if (state == SlimeState.inWater)
            {
                jumpHeight = 350;
                targetHealth = -1f;
            }
            else if (state == SlimeState.Electricity)
            {
                jumpHeight = 500;
                light = true;
            }
            stateChanged = false;
        }

        if (targetHealth != size && targetHealth != -1f)
        {
            size -= 1f * Time.deltaTime;
        }
        if (targetMoveSpeed != moveSpeed)
        {
            if (targetMoveSpeed > moveSpeed) moveSpeed += 0.1f * Time.deltaTime;
            else moveSpeed -= 0.1f * Time.deltaTime;
        }
        gameObject.transform.localScale = new Vector3(0.05f * Mathf.Sqrt(size), 0.05f * Mathf.Sqrt(size), 0.005f * size);
    }
    #endregion

    public void Reset()
    {
        speed = 0;
        size = 100;
        ChangeState(SlimeState.Normal,true);
        spiked = false;
        swipeMoveLeft = false;
        swipeMoveRight = false;
        swipeMoveUp = false;
        animationDelay = 0.5f;
        if (otherPlayer != null)
        {
            setActive();
            Destroy(otherPlayer.gameObject);
        }
    }

    void ResetLevel()
    {
        if (!spiked)
        {
            this.GetComponent<SlimeTrail>().RemoveSplats();
            LevelHandler.instance.ResetLevelPostSound();
        }
        else
        {
            this.GetComponent<SlimeTrail>().RemoveSplats();
            LevelHandler.instance.ResetLevel();
        }
    }

    public void HitGoal()
    {
        
        if (CanvasHandler.instance.CurrentScreen != null)
            return;
        AudioManager.instance.PlaySound(AudioManager.SoundClips.Victory, false);
        animationDelay = 0.5f;
        EndscreenHandler.instance.ShowScreen(size);
    }

    void CheckDeath()
    {
        if (spiked||size <= 25 || gameObject.transform.position.y < -7)
        {
            if (otherPlayer != null)
            {
                otherPlayer.setActive();
                otherPlayer.GetComponent<SlimeTrail>().AddSplats(this.GetComponent<SlimeTrail>().Trail);
                otherPlayer.otherPlayer = null;
                Destroy(this.gameObject);
            }
            else
            {
                ResetLevel();
            }
        }
    }
	public void Die(){
		if (otherPlayer != null)
		{
			otherPlayer.setActive();
			otherPlayer.GetComponent<SlimeTrail>().AddSplats(this.GetComponent<SlimeTrail>().Trail);
			otherPlayer.otherPlayer = null;
			Destroy(this.gameObject);
		}
		else
		{
			ResetLevel();
		}
	}

    public RaycastHit getGroundCollisionPoint(Vector3 rayDirection = new Vector3())
    {
        if (rayDirection == new Vector3()) rayDirection = new Vector3(0, -1, 0);
		RaycastHit rayInfo;
		Physics.Raycast(this.transform.position, rayDirection, out rayInfo);
		if(this.transform.position.y - rayInfo.point.y - this.GetComponent<BoxCollider>().size.y/2.0f * transform.localScale.y <= 1.3f && rayInfo.collider.gameObject){
            if (!rayInfo.collider.gameObject.CompareTag("Splat") && !rayInfo.collider.gameObject.CompareTag("InteractableObject") &&  (!rayInfo.collider.gameObject.CompareTag("Player")))
            {
                
				prevPos = rayInfo.point;
                return rayInfo;
			}
            else
            {
                
                if (!rayInfo.collider.gameObject.CompareTag("InteractableObject"))
                {
                    if (rayInfo.collider.GetComponent<Renderer>() != null)
                    {
                        this.GetComponent<SlimeTrail>().setSplatColor(rayInfo.collider.gameObject);
                    }
                }
                return new RaycastHit();
			}
		}
        else
        {
            return new RaycastHit();
		}
    }


    private float mergeTimer = 1f;
    private float maxMergeTimer = 1f;

    private void resetMergeTimer()
    {
        //Debug.Log("resetingTimer");
        mergeTimer = maxMergeTimer;
    }

    private bool mayMerge()
    {
        if (Vector3.Distance(this.transform.position, otherPlayer.transform.position) < 0.4f)
        {
            mergeTimer -= Time.deltaTime;
            if (mergeTimer <= 0)
            {
                mergeTimer = maxMergeTimer;
                return true;
            }

        }
        else
        {
            resetMergeTimer();
        }
       return false; 
    }

    public void Merge()
    {
        size += otherPlayer.size;
        maxSize = 100;
        if (otherPlayer.state != state)
        {
            ChangeState(SlimeState.Normal);
        }
        otherPlayer.otherPlayer = null;
        Destroy(otherPlayer.gameObject);
        Destroy(otherPlayer);
        //otherPlayer = null;
    }
    
   

}
