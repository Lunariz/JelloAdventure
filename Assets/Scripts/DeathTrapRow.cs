﻿using UnityEngine;
using System.Collections;

public class DeathTrapRow : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision e)
    {
        PlayerController pc = e.gameObject.GetComponent<PlayerController>();
        if (pc!= null)
        {
            pc.spiked = true;
        }
    }
}
