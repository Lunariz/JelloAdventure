﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Trigger : MonoBehaviour
{
    public SlimeState stateToBreakObject;
    public List<TriggerableObject> triggers;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider e)
    {
        PlayerController p = e.GetComponent<PlayerController>();
        if (p != null)
        {
            if (p.state == stateToBreakObject)
            {
                if (stateToBreakObject == SlimeState.Fire)
                {
                    AudioManager.instance.PlaySound(AudioManager.SoundClips.BurnRope, false);
                }
                foreach (TriggerableObject to in triggers)
                {
                    if(to.gameObject != null)to.OnTriggered();
                }

                Destroy(this.gameObject);
            }
        }
    }
}
