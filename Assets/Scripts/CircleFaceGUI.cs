﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CircleFaceGUI : MonoBehaviour {

    public GameObject Happyface;
    public GameObject Normalface;
    public GameObject Sadface;

    public Color fullcolor;
    public Color halfcolor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void ShowFace(GameObject face)
    {
        Happyface.SetActive(false);
        Normalface.SetActive(false);
        Sadface.SetActive(false);
        if (face) {
            face.SetActive(true);
            this.GetComponent<Image>().color = fullcolor;
        }
        else {
            Color currentColor = this.GetComponent<Image>().color;
            this.GetComponent<Image>().color = halfcolor;
        }
    }
}
