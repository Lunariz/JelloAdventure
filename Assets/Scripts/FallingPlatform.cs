﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : TriggerableObject {
    Rigidbody r;
	private bool falling = false;
	private Vector3 prevPos;

    void Awake()
    {
        r = GetComponent<Rigidbody>();
		prevPos = this.gameObject.transform.position;
    }
	// Use this for initialization
	void Start () {
	
	}

    public override void OnTriggered()
    {
        r.isKinematic = false;
        r.useGravity = true;
		falling = true;
    }

	void OnCollisionEnter(Collision collision){
		if(collision.gameObject.tag == "Player" && falling){
			collision.gameObject.GetComponent<PlayerController>().Die();
		}else{
			//falling = false;
		}
	}
}
