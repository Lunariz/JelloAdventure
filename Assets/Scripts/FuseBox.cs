﻿using UnityEngine;
using System.Collections;

public class FuseBox : MonoBehaviour {
    public ExitDoor toUnlock;
    public GameObject enableParticle;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider e)
    {

        PlayerController pc = e.gameObject.GetComponent<PlayerController>();
        if (pc == null) return;
        if (pc.state == SlimeState.Electricity)
        {
            AudioManager.instance.PlaySound(AudioManager.SoundClips.FuseBox, false);
            enableParticle.SetActive(true);
            pc.ChangeState(SlimeState.Normal);
            toUnlock.OpenDoor();
        }
    }
}
