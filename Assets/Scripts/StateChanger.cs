﻿using UnityEngine;
using System.Collections;

//attached to collidebale objects not player
public enum SlimeState
{
	Normal = 0,
    Oil = 1,
    Fire = 2,
    Electricity = 3,
    Water = 4,
	inWater = 5
}

public class StateChanger : MonoBehaviour {
    public SlimeState Source;

}
