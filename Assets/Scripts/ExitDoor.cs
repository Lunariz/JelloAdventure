﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExitDoor : MonoBehaviour {
    public bool startOpen;
    public ExitSign exitSign;

    private bool open;
    private Animator anim;

    [HideInInspector]
    public List<FuseBox> fuseboxes;

   
	// Use this for initialization
	void Start () {
        anim = this.GetComponent<Animator>();
        exitSign.setDoor(startOpen);
        open = startOpen;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OpenDoor()
    {
        exitSign.setDoor(true);
        open = true;
    }

    void OnTriggerEnter(Collider e)
    {
        if (open)
        {
            anim.Play("door open");

            e.gameObject.GetComponent<PlayerController>().Invoke("HitGoal", 0.7f) ;
        }

    }
}
