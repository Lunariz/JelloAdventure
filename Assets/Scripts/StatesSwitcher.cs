﻿using UnityEngine;
using System.Collections;

public class StatesSwitcher : MonoBehaviour {
	public PlayerController playerControllerScript;

	private SlimeState currentState;
	// Use this for initialization
	void Start () {
		currentState = playerControllerScript.state;
	}

	void OnTriggerEnter  (Collider collision) {

		currentState = playerControllerScript.state;
		if(collision.gameObject.GetComponent<StateChanger> () != null){
			canSwitchFromTo(currentState, collision.gameObject.GetComponent<StateChanger> ().Source);
		}

	}

    //bool isSwimming = false;
    void OnTriggerStay(Collider e)
    {
        StateChanger sc = e.GetComponent<StateChanger>();
        if (sc == null) return;
        if( sc.Source == SlimeState.Water)
            playerControllerScript.Swimming(true);
    }

	void OnTriggerExit (Collider collision){
		if(collision.gameObject.GetComponent<StateChanger> () != null){
            SlimeState s = collision.gameObject.GetComponent<StateChanger> ().Source;
            if (s == SlimeState.Water)
            {
                playerControllerScript.Swimming(false);
            }
			if(s == SlimeState.Water && playerControllerScript.state == SlimeState.inWater){
				playerControllerScript.ChangeState( SlimeState.Water);
			}
		}

	}

    

    public void canSwitchFromTo(SlimeState from, SlimeState to)
    {
		if(from != to){ 
			//AudioManager.instance.
			//if normal then change to the other state if it's not fire
			if(from == SlimeState.Normal && to != SlimeState.Fire && to!=SlimeState.Electricity){
				if(to == SlimeState.Water){
					playerControllerScript.ChangeState(SlimeState.inWater);
                    AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToWater, false);
				}else{
                    if (to == SlimeState.Oil)
                    {
                        AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToOil, false);
                    }
					playerControllerScript.ChangeState(to);
				}

			}
			//if oil then get on fire if fire/charged
			if(from == SlimeState.Oil && to != SlimeState.Water){
				playerControllerScript.ChangeState(SlimeState.Fire);
                AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToFire,false);
			}
			if(from == SlimeState.Fire && to == SlimeState.Water){
				playerControllerScript.ChangeState(SlimeState.inWater);
                AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToWater, false);
			}
			if(from == SlimeState.Water && to == SlimeState.Electricity){
				playerControllerScript.ChangeState(to);
                AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToElectric, false);
			}

			if(from == SlimeState.Electricity && to == SlimeState.Oil){
				playerControllerScript.ChangeState(to);
                AudioManager.instance.PlaySound(AudioManager.SoundClips.SwitchToOil, false);
			}
		}else{
			if(from == SlimeState.Water && to == SlimeState.Water) {
				playerControllerScript.ChangeState(SlimeState.inWater);
			}
		}
    }


}
