﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WaterBlock : MonoBehaviour {
    public List<GameObject> waterblocks = new List<GameObject>();
    private GameObject movingWater;
    private BoxCollider movingWaterCollider;

    public void loadEditorData()
    {
        if (GetComponentInChildren<WaterColiderScript>() == null)
        {
            movingWater = GameObject.Instantiate(Resources.Load("Water/MovingWater")) as GameObject;
            movingWater.transform.parent = transform;
            
        }
        //movingWater = GetComponentInChildren<WaterColiderScript>().gameObject;
        if (movingWater == null)
        {
            movingWater = GameObject.Instantiate(Resources.Load("Water/MovingWater")) as GameObject;
            movingWater.transform.parent = transform;
        }
        movingWaterCollider = movingWater.GetComponent<BoxCollider>();
    }
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void addBlock()
    {
        loadEditorData();
        GameObject wb = GameObject.Instantiate(Resources.Load("Water/scalable-water-body"))as GameObject;
        wb.transform.parent = transform;
        waterblocks.Add(wb);
        repositionWaterBlocks();

        

    }

    public void RemoveBlock()
    {
        loadEditorData();
        GameObject wb = waterblocks[waterblocks.Count - 1];
        waterblocks.Remove(wb);
        DestroyImmediate(wb,true);
        repositionWaterBlocks();

    }

    private void repositionWaterBlocks()
    {
        int i = waterblocks.Count;
        int startPos = 0;
        movingWaterCollider.size = new Vector3(i, movingWaterCollider.size.y, movingWaterCollider.size.z);
        if (i % 2 == 0)
        {
            // even number of waterblocks moving water colider should be in between;
            movingWaterCollider.center = new Vector3(-.5f, 0, -.5f);


        }
        else
        {
            //odd number of waterblocks moving water should be on a block
            movingWaterCollider.center = new Vector3(0, 0, -.5f);

        }
        startPos = (-Mathf.FloorToInt(i / 2));

        for (int ix = 0; ix < i; ix++)
        {
            waterblocks[ix].transform.localPosition = new Vector3(startPos + ix, waterblocks[0].transform.localPosition.y, waterblocks[0].transform.localPosition.z);

        }
    }
}
