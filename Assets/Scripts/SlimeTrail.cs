﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlimeTrail : MonoBehaviour {

	private Vector3 currentPos;
	private Vector3 prevPos;
	private Vector3 prevPosSide;
	private float NoClip = 0.01f;
	private float NoClip2 = 0.02f;
	float clip;
	public PlayerController playerControllerScript;
	public GameObject slimePref;
	public List<Color32>RGBList1;
	public List<GameObject> Trail;
	/*private Color32[] RGBList = new Color32[6]{new Color(255, 255, 255, 255), new Color(27, 29, 61, 255), new Color (255, 1, 6, 255),
		new Color(255, 255, 0, 255), new Color(115, 137, 255, 255), new Color(115, 137, 255, 255)};*/


	// Use this for initialization
	void Start () {
		InvokeRepeating("LeaveSplat", 0.0f, 0.25f);
	}

	void LeaveSplat () {
        if (CanvasHandler.instance.CurrentScreen != null) return;
        RaycastHit hitPoint = playerControllerScript.getGroundCollisionPoint();
        Vector3 currentPosVec3 = hitPoint.point;
        if (currentPosVec3 == new Vector3(0, 0, 0)) 
            return;
        currentPos = currentPosVec3;
		if(currentPosVec3 != prevPos)
        {
			if(NoClip != NoClip2)
            {
				clip = NoClip;
				NoClip = NoClip2;
				NoClip2 = clip;
			}
			prevPos = currentPosVec3;
			GameObject slime = Instantiate(slimePref, new Vector3(currentPos.x, currentPos.y + + NoClip, currentPos.z), Quaternion.Euler(75, 180, 180)) as GameObject;
			int myinto = (int)playerControllerScript.state;
			slime.GetComponent<Renderer>().material.color  = RGBList1[myinto];
            slime.transform.localScale = new Vector3(0.5f + 0.5f * Random.value, 0.5f + 0.5f * Random.value, 0.5f + 0.5f * Random.value);
            slime.transform.parent = hitPoint.collider.gameObject.transform;
            slime.transform.LookAt(hitPoint.point + -hitPoint.normal);
			Trail.Add(slime);
            gameObject.GetComponent<PlayerController>().size -= 0.5f;
		}
	}

	public void LeaveSplatSide(Vector4 pos, GameObject hitObject) //experimental on ceiling: , Vector3 bounceDirection)
    {
        //experimental on ceiling
        //RaycastHit hitPoint = playerControllerScript.getGroundCollisionPoint(bounceDirection);
        //Vector3 lookatPoint = hitPoint.point;
		Vector3 currentPosVec3 = new Vector3(pos.x, pos.y, pos.z);
		if(NoClip != NoClip2){
			clip = NoClip;
			NoClip = NoClip2;
			NoClip2 = clip;
		}
		prevPosSide = currentPosVec3;
		GameObject slime = Instantiate(slimePref, new Vector3(pos.x, pos.y, pos.z), Quaternion.identity) as GameObject;;
        if (pos.w == -1)
        {
            slime.transform.rotation = Quaternion.Euler(0, 180, 0);
            slime.transform.position = new Vector3(slime.transform.position.x, slime.transform.position.y, slime.transform.position.z + NoClip);
        }
		if(pos.w == 1)
        {
            slime.transform.rotation = Quaternion.Euler(0, 180, 90);
			slime.transform.position = new Vector3(slime.transform.position.x, slime.transform.position.y, slime.transform.position.z - NoClip);
		}
		int myinto = (int) playerControllerScript.state;
		slime.GetComponent<Renderer>().material.color  = RGBList1[myinto];
        slime.transform.localScale = new Vector3(0.5f + 0.5f * Random.value, 0.5f + 0.5f * Random.value, 0.5f + 0.5f * Random.value);
        slime.transform.parent = hitObject.transform;
        //experimental slime on ceiling
        //slime.transform.LookAt(lookatPoint + -hitPoint.normal);
		Trail.Add(slime);
        gameObject.GetComponent<PlayerController>().size -= 0.5f;
	}

	public void setSplatColor (GameObject splat){
        
		splat.GetComponent<Renderer>().material.color  = RGBList1[(int)playerControllerScript.state];
	}

    public void RemoveSplats()
    {
        foreach (GameObject splat in Trail)
        {
            Destroy(splat);
        }
        Trail = new List<GameObject>();
    }

    public void AddSplats(List<GameObject> splats)
    {
        Trail.AddRange(splats);
    }
}
