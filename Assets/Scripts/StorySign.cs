﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class StorySign : MonoBehaviour {
    public static StorySign instance = null;
    public Text textfield;

    public List<string> introStory;
    int curStorypoint = 0;

    void Awake()
    {
        instance = this;
        this.gameObject.SetActive(false);
    }
	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Input.GetKeyDown(KeyCode.X))
            {
                OnTap();
            }
        }
	}

    public void ShowStory(string text)
    {
        textfield.text = text;
        CanvasHandler.instance.ShowScreen(CanvasHandler.instance.TutorialScreen);
    }

    public void ShowIntroductions()
    {
        if (introStory.Count != 0)
        {
            //this.gameObject.SetActive(true);
            curStorypoint = 0;
            textfield.text = introStory[0];
            CanvasHandler.instance.ShowScreen(CanvasHandler.instance.TutorialScreen);
        }
        else
        {
            CanvasHandler.instance.ShowScreen(null);
        }
    }

    public void OnTap()
    {
        if (curStorypoint >= introStory.Count-1)
        {
            foreach (PlayerController p in FindObjectsOfType<PlayerController>())
            {
                p.animationDelay = .3f;
            }
            
        }
        else
        {
            curStorypoint += 1;
            ShowStory(introStory[curStorypoint]);
            return;
        }
        curStorypoint = 0;
        introStory.Clear();
        CanvasHandler.instance.ShowScreen(null);
        
    }

    List<string> ingameLines;
    public void SetData(string line)
    {
        this.gameObject.SetActive(true);
        introStory.Add(line);
    }
}
