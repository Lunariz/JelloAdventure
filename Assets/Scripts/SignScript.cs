﻿using UnityEngine;
using System.Collections;

public class SignScript : MonoBehaviour {

    [Multiline]
    public string pcText;
    [Multiline]
    public string phoneText;
    
    
    public string TextOnPost;


    public Material matForKeyboardInput;
    public Renderer sign;

    public bool allowKeyInput;
    public bool mouseOver;

    void Awake()
    {
        
        TextOnPost = phoneText;
        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            sign.material = matForKeyboardInput;
            TextOnPost = pcText;

        }
        StorySign.instance.SetData(TextOnPost);
    }



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (allowKeyInput) //&& Application.platform == RuntimePlatform.WebGLPlayer)
        {
            if (Input.GetKeyDown(KeyCode.X) && !CanvasHandler.instance.CurrentScreen)
            {
                StorySign.instance.ShowStory(TextOnPost);
            }
        }
        if (mouseOver && Input.touchCount > 0 && Input.touches[0].phase == TouchPhase.Ended && !CanvasHandler.instance.CurrentScreen)
            StorySign.instance.ShowStory(TextOnPost);
	
	}

    void OnMouseEnter()
    {
        mouseOver = true;
    }

    void OnMouseExit()
    {
        mouseOver = false;
    }



    void OnTriggerEnter(Collider e)
    {
        if (e.gameObject.GetComponent<PlayerController>().Active)
        {
            allowKeyInput = true;
        }
    }

    void OnTriggerExit(Collider e)
    {
        if (e.gameObject.GetComponent<PlayerController>().Active)
        {
            allowKeyInput = false;
        }
    }
}
