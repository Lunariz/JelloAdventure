﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CanvasHandler : MonoBehaviour {

    public static CanvasHandler instance;
    public GameObject StartMenu;
    public GameObject EndScreen;
    public GameObject CreditScreen;
    public GameObject LevelSelect;
    public GameObject TutorialScreen;
    public GameObject PauseScreen;

    public GameObject CurrentScreen;

    public void ShowScreen(GameObject screen)
    {
        if (screen == StartMenu) AudioManager.instance.PlayMusic(GameSceneState.Menu);
        if (CurrentScreen) CurrentScreen.SetActive(false);
        if (screen) {
            screen.SetActive(true);
			CurrentScreen = screen;
        }
        CurrentScreen = screen;
    }

    public void CloseScreen()
    {
        if (CurrentScreen) CurrentScreen.SetActive(false);
        CurrentScreen = null;
    }

    void Start()
    {
        EndScreen.SetActive(true);
        TutorialScreen.SetActive(true);
        instance = this;
        ShowScreen(StartMenu);
        
    }

    public void BackToMainMenu()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

	// Update is called once per frame
	void Update () {
        if (!CurrentScreen && Input.GetKey("p"))
        {
            ShowScreen(PauseScreen);
        }
	}
}
