﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Rope : TriggerableObject {
    public TriggerableObject onTriggered;
    public float burnTime;
    public delegate void Burn();
    public Burn burn = null;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


    public override void OnTriggered()
    {
        if (burn != null) burn();
        Invoke("onBurned",burnTime);
    }

    public void onBurned(){
        if(onTriggered!=null) onTriggered.OnTriggered();
        Destroy(this.gameObject);
    }
}
