﻿using UnityEngine;
using System.Collections;

public class TeleporScript : MonoBehaviour {

	private PipeSystem pipeSystemScript;
	private int currentPipe = 1;
	private GameObject player;
	private float cameraZpos;
	private float fakeZPos;
	public float speed = 0.002f;
	private bool inPipe = false;
	
	void Start () {
		GameObject _parent = this.gameObject;
		while(pipeSystemScript == null){
			_parent = _parent.transform.parent.gameObject;
			pipeSystemScript = _parent.GetComponent<PipeSystem>();
		}
		cameraZpos = Camera.main.gameObject.transform.position.z;
		fakeZPos = cameraZpos;
	}

	void OnTriggerEnter(Collider _collider){
		//Debug.Log(pipeSystemScript.activated);
		if(_collider.gameObject.tag == "Player" && pipeSystemScript.activated == true){
			player = _collider.gameObject;
			//Debug.Log(Camera.main.gameObject.transform.position.z);
			Camera.main.GetComponent<FollowPlayer>().enabled = false;
			_collider.gameObject.SetActive(false);
			inPipe = true;
		}
	}

	void Update(){
		if(inPipe){
			if( currentPipe < pipeSystemScript.pipes.Count){
				//Debug.Log(currentPipe);
				Vector3 camPos = Camera.main.gameObject.transform.position;
				camPos.z = fakeZPos;
				Vector3 pipePos = pipeSystemScript.pipes[currentPipe].transform.position;
				//Debug.Log(Vector3.Distance(camPos, pipePos));
				if(Vector3.Distance(camPos, pipePos) > 0.1f){
					camPos = Vector3.MoveTowards(camPos, pipePos, 0.1f);
					fakeZPos = camPos.z;
					camPos.z = cameraZpos;
					Camera.main.gameObject.transform.position = camPos;
				}else{
					currentPipe++;
				}
				/*
			Camera.main.gameObject.transform.position = Vector3.Lerp(Camera.main.gameObject.transform.position, pipeSystemScript.pipes[currentPipe].transform.position,  0.01f);
			Camera.main.gameObject.transform.position = new Vector3(Camera.main.gameObject.transform.position.x, Camera.main.gameObject.transform.position.y, _cameraZpos);
			currentPipe++;*/
			}else{
				player.transform.position = pipeSystemScript.pipes[currentPipe-1].transform.position;
				player.SetActive(true);
				Camera.main.GetComponent<FollowPlayer>().enabled = true;
				currentPipe = 1;
				//CancelInvoke("StartCameraMovement");
				inPipe = false;
			}
		}
	}

}
