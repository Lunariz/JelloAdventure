﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class HUDHandler : MonoBehaviour {

    public GameObject PlayerSlider;
    public GameObject Player1Slider;
    public GameObject Player2Slider;
    public GameObject Switch;
    

    public static HUDHandler instance;

    public void SetSlider(float fill1, float fill2 = -1) {
        if (fill2 == -1)
        {
            PlayerSlider.SetActive(true);
            Player1Slider.SetActive(false);
            Player2Slider.SetActive(false);
            Switch.SetActive(false);
            PlayerSlider.GetComponent<Image>().fillAmount = (fill1-25)/75f;
        }
        else
        {
            PlayerSlider.SetActive(false);
            Player1Slider.SetActive(true);
            Player2Slider.SetActive(true);
            Switch.SetActive(true);
            Player1Slider.GetComponent<Image>().fillAmount = (fill1-25)/25f;
            Player2Slider.GetComponent<Image>().fillAmount = (fill2-25)/25f;
        }
    }

    public void SetColors(Color color1, Color color2 = new Color())
    {
        if (Player1Slider.activeSelf)
        {
            Player1Slider.GetComponent<Image>().color = color1;
            Player2Slider.GetComponent<Image>().color = color2;
        }
        else {
            PlayerSlider.GetComponent<Image>().color = color1;
        }
    }

    void Awake()
    {
        instance = this;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
